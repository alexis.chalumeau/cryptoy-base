from cryptography.hazmat.primitives.ciphers.aead import (
    AESGCM,
)


def encrypt(msg: bytes, key: bytes, nonce: bytes) -> bytes:
    # A implémenter en utilisant la class AESGCM
    crypt_data = AESGCM(key).encrypt(nonce, msg, None)
    return crypt_data


def decrypt(msg: bytes, key: bytes, nonce: bytes) -> bytes:
    # A implémenter en utilisant la class AESGCM
    sha = AESGCM(key)
    data = AESGCM(key).decrypt(nonce, msg, None)
    return data
