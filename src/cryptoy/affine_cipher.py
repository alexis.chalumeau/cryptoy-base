from math import (
    gcd,
)

from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)

# TP: Chiffrement affine


def compute_permutation(a: int, b: int, n: int) -> list[int]:
    # A implémenter, en sortie on doit avoir une liste result tel que result[i] == (a * i + b) % n
    result = []

    for i in range(n):
        result.append((a*i+b)%n)
    
    return result


def compute_inverse_permutation(a: int, b: int, n: int) -> list[int]:
    # A implémenter, pour cela on appelle perm = compute_permutation(a, b, n) et on calcule la permutation inverse
    # result qui est telle que: perm[i] == j implique result[j] == i
    """result = [0] * n
    prov = compute_permutation(a, b, n)
    for i in range(len(result)):
        result[i] = prov.index(i)
    return result"""
    result = [0] * n
    prov = compute_permutation(a, b, n)
    for i in range(len(prov)):
        result[prov[i]] = i
    return result
    

def encrypt(msg: str, a: int, b: int) -> str:
    # A implémenter, en utilisant compute_permutation, str_to_unicodes et unicodes_to_str
    uni_msg = str_to_unicodes(msg)

    perm = compute_permutation(a, b, 0x110000)

    for i in range(len(uni_msg)):
        uni_msg[i] = perm[uni_msg[i]]

    encrypt_msg = unicodes_to_str(uni_msg)

    return(encrypt_msg)


def encrypt_optimized(msg: str, a: int, b: int) -> str:
    # A implémenter, sans utiliser compute_permutation
    uni_msg = str_to_unicodes(msg)

    for i in range(len(uni_msg)):
        uni_msg[i] = ((a*uni_msg[i]+b)%0x110000)

    encrypt_msg = unicodes_to_str(uni_msg)

    return encrypt_msg


def decrypt(msg: str, a: int, b: int) -> str:
    # A implémenter, en utilisant compute_inverse_permutation, str_to_unicodes et unicodes_to_str
    uni_msg = str_to_unicodes(msg)

    perm = compute_inverse_permutation(a, b, 0x110000)
    print(len(uni_msg))
    for i in range(len(uni_msg)):
        uni_msg[i] = perm[uni_msg[i]]

    encrypt_msg = unicodes_to_str(uni_msg)
    print(encrypt_msg)
    return(encrypt_msg)


def decrypt_optimized(msg: str, a_inverse: int, b: int) -> str:
    # A implémenter, sans utiliser compute_inverse_permutation
    # On suppose que a_inverse a été précalculé en utilisant compute_affine_key_inverse, et passé
    # a la fonction
    uni_msg = str_to_unicodes(msg)

    for i in range(len(uni_msg)):
        uni_msg[i] = (a_inverse*(uni_msg[i]-b)%0x110000)

    decrypt_msg = unicodes_to_str(uni_msg)
    
    return decrypt_msg


def compute_affine_keys(n: int) -> list[int]:
    # A implémenter, doit calculer l'ensemble des nombre a entre 1 et n tel que gcd(a, n) == 1
    # c'est à dire les nombres premiers avec n
    result = []
    for i in range(1,n):
        if gcd(i, n) == 1:
            result.append(i)
    return result


def compute_affine_key_inverse(a: int, affine_keys: list, n: int) -> int:
    # Trouver a_1 dans affine_keys tel que a * a_1 % N == 1 et le renvoyer
    # Placer le code ici (une boucle)
    for key in affine_keys:
        if ((a*key)%n) == 1:
            return key
    # Si a_1 n'existe pas, alors a n'a pas d'inverse, on lance une erreur:
    raise RuntimeError(f"{a} has no inverse")


def attack() -> tuple[str, tuple[int, int]]:
    s = "࠾ੵΚઐ௯ஹઐૡΚૡೢఊஞ௯\u0c5bૡీੵΚ៚Κஞїᣍફ௯ஞૡΚր\u05ecՊՊΚஞૡΚՊեԯՊ؇ԯրՊրր"
    # trouver msg, a et b tel que affine_cipher_encrypt(msg, a, b) == s
    # avec comme info: "bombe" in msg et b == 58
    a = 1
    msg = ""
    affine_keys = compute_affine_keys(0x110000)
    while a <=100:
        try:
            a_1 = compute_affine_key_inverse(a, affine_keys, 0x110000)
        except:
            a += 1
            continue
        msg = decrypt_optimized(s, a_1, 58)
        #print("msg : "+msg)
        if "bombe" in msg:
            break
        a += 1
    return (msg,(a,58))


    # Placer le code ici

    raise RuntimeError("Failed to attack")


def attack_optimized() -> tuple[str, tuple[int, int]]:
    s = (
        "જഏ൮ൈ\u0c51ܲ೩\u0c51൛൛అ౷\u0c51ܲഢൈᘝఫᘝా\u0c51\u0cfc൮ܲఅܲᘝ൮ᘝܲాᘝఫಊಝ""0c64\u0c64ൈᘝࠖܲೖఅܲఘഏ೩ఘ\u0c51ܲ\u0c51൛൮ܲఅ\u0cfc\u0cfcඁೖᘝ\u0c51"
    )
    # trouver msg, a et b tel que affine_cipher_encrypt(msg, a, b) == s
    # avec comme info: "bombe" in msg
    a = 1
    msg = ""
    affine_keys = compute_affine_keys(0x110000)
    print("check")
    while a <= 100:
        a_1 = 0
        try:
            a_1 = compute_affine_key_inverse(a, affine_keys, 0x110000)
        except:
            print("error")
            a += 1
            continue
        b = 1
        print("ok")
        while b <= 10000:
            msg = decrypt_optimized(s, a_1, b)
            print("msg : " + msg)
            if "bombe" in msg:
                return (msg, (a, b))
            b += 1
        a += 1

    # Placer le code ici

    raise RuntimeError("Failed to attack")
