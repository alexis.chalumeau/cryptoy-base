from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)

# TP: Chiffrement de César


def encrypt(msg: str, shift: int) -> str:
    # Implémenter le chiffrement de César
    # Il faut utiliser la fonction str_to_unicodes, puis appliquer la formule
    # (x + shift) % 0x110000 pour chaque unicode du tableau puis utiliser
    # unicodes_to_str pour repasser en string
    uni_msg = str_to_unicodes(msg)
    for i in range(len(uni_msg)):
        uni_msg[i] = (uni_msg[i] + shift) % 0x110000
    encrypt_msg = unicodes_to_str(uni_msg)
    return encrypt_msg


def decrypt(msg: str, shift: int) -> str:
    # Implémenter le déchiffrement. Astuce: on peut implémenter le déchiffrement en
    # appelant la fonction de chiffrement en modifiant légèrement le paramètre
    return encrypt(msg, -shift)


def attack() -> tuple[str, int]:
    s = "恱恪恸急恪恳恳恪恲恮恸急恦恹恹恦恶恺恪恷恴恳恸急恵恦恷急恱恪急恳恴恷恩怱急恲恮恳恪恿急恱恦急恿恴恳恪"
    # Il faut déchiffrer le message s en utilisant l'information:
    # 'ennemis' apparait dans le message non chiffré

    # Code a placer ici, il faut return un couple (msg, shift)
    # ou msg est le message déchiffré, et shift la clef de chiffrage correspondant

    ENNEMIS = "ennemis"

    uni_s = str_to_unicodes(s)

    check = 0

    shift = 0

    for i in range(len(uni_s)):
        if uni_s[i] == uni_s[i+3] and uni_s[i+1] == uni_s[i+2]:
            shift = uni_s[i] - ord('e') % 0x110000
            cpt_check = 0
            for j in range(i,i+len("ennemis")):
                if (uni_s[j] - shift % 0x110000) != ENNEMIS[cpt_check]:
                    continue
            check = 1
            break

    if check:
        print(decrypt(s, shift))
        return (decrypt(s,shift),shift)
    else:
        # Si on ne trouve pas on lance une exception:
        raise RuntimeError("Failed to attack")
